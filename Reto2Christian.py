#Christian Londono Canas
import cv2 as cv
import cv2
import imutils
import numpy as np
import scipy as sp
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans
from time import time
from sklearn import cluster, datasets
from cv2 import boundingRect, countNonZero, cvtColor, drawContours, findContours, getStructuringElement, imread, morphologyEx, pyrDown, rectangle, threshold, circle, putText

def readImg(location):
    File = cv.imread(str(location))
    File = cv.cvtColor(File, cv.COLOR_BGR2LAB)
    l,a,b = cv.split(File)
    l[...] = 255
    File = cv.merge([l,a,b])
    File = cv.GaussianBlur(File,(11,11),0)
    return File

Time =time()
Imagen = readImg("FotosTamanooptimo/3.jpg")
(h, w) = Imagen.shape[:2]
Imagen = Imagen.reshape((-1, 3))
print("Leyendo imagen realizado en: " + str(time()-Time))
Time =time()
k = KMeans(n_clusters = 6)
labels = k.fit_predict(Imagen)
quant = k.cluster_centers_.astype("uint8")[labels]
quant = quant.reshape((h, w, 3))
quant = cv.cvtColor(quant, cv.COLOR_LAB2BGR)
print("Closterizando realizado en: " + str(time()-Time))
Time = time()
rgb = pyrDown(quant)
small = cvtColor(rgb, cv2.COLOR_BGR2GRAY)
_, bw = threshold(src=small, thresh=0, maxval=255, type=cv2.THRESH_BINARY+cv2.THRESH_OTSU)
morph_kernel = getStructuringElement(cv2.MORPH_RECT, (9, 1))
connected = morphologyEx(bw, cv2.MORPH_CLOSE, morph_kernel)
mask = np.zeros(bw.shape, np.uint8)
im2, contours, hierarchy = findContours(connected, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
colorDict = {}
dict = {}
for idcontor in range(0, len(hierarchy[0])):
    color = ()
    rect = x, y, rect_width, rect_height = boundingRect(contours[idcontor])
    mask = drawContours(mask, contours, idcontor, (255,255,255), cv2.FILLED)
    r = float(countNonZero(mask)) / (rect_width * rect_height)
    if r > 0.45 and rect_height > 8 and rect_width > 8 and hierarchy[0,idcontor,3]!=-1:
        temp = rgb[(y+rect_height/2),(x+rect_width/2)]
        color = np.array((int(temp[0]),int(temp[1]),int(temp[2])))
        rgb = circle(rgb,(x+rect_width/2, y+rect_height/2), 20, color, -1)
        if str(temp) in dict :
            dict[str(temp)]+=1
        else:
            colorDict[str(temp)] = temp
            dict[str(temp)]=1
        rgb = putText(rgb,str(dict[str(temp)]),(x+rect_width/2, y+rect_height/2), cv2.FONT_HERSHEY_DUPLEX, 0.5, (0,0,0))
print("Connected component" + str(Time-time()))
Data = np.zeros((len(dict)*60,200,3), np.uint8)
Data = Data+1
Data = Data*255
center = (20,20)
L = (60, 20)
Y = 20
color = ()
for key in colorDict:
    temp = colorDict[key]
    color = (int(temp[0]),int(temp[1]),int(temp[2]))
    Data = circle(Data, center, 20, color , -1)
    Data = putText(Data,str(dict[str(temp)]),L, cv2.FONT_HERSHEY_DUPLEX, 0.5, (0,0,0))
    Y = Y + 50
    center = (20, Y)
    L = (60, Y)

cv.imshow("Imagen marcada", rgb)
cv.imshow("Informacion en tabla", Data)
cv.waitKey(0)

